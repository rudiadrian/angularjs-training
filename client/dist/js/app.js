(function () {
    'use strict';

    angular
        .module('app', [
            /* Shared modules */
            'app.core',
            'app.widgets',

            /* App modules */
            'app.todos'
        ]);
}());

(function () {
    'use strict';

    angular
        .module('app.core', [
            /* Angular modules */

            /* Cross-app modules */

            /* 3rd-party modules */
            'ui.router',
            'ngResource'
        ]);
}());

(function () {
    'use strict';

    angular
        .module('layout', [
            'app.core'
        ]);
}());

(function () {
    'use strict';

    angular
        .module('app.todos', [
            'app.core',
            'app.widgets'
        ]);
}());

(function () {
    'use strict';

    angular
        .module('app.widgets', [
        ]);
}());

(function () {
    'use strict';

    angular
        .module('app.core')
        .constant('paths', {
            TEMPLATES: 'templates',
            API: 'http://localhost:3000'
        });
}());

(function () {
    'use strict';

    angular
        .module('app')
        .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider', 'paths'];

    function configure ($stateProvider, $urlRouterProvider, paths) {
        $stateProvider
            .state('app', {
            url: '/app',
                abstract: true,
                templateUrl: paths.TEMPLATES + '/pages/app.html',
            });

        $urlRouterProvider.otherwise('/app/todos/list');
    }
}());

(function () {
    'use strict';

    angular
        .module('app.todos')
        .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider', 'paths'];

    function configure ($stateProvider, $urlRouterProvider, paths) {
        $stateProvider
            .state('app.todos', {
                url: '/todos',
                template: '<ui-view/>',
                abstract: true
            })
            .state('app.todos.list', {
                url: '/list',
                templateUrl: paths.TEMPLATES + '/views/todos.html',
                controller: 'TodosController',
                controllerAs: 'todos'
            })
            .state('app.todos.detail', {
                url: '/detail/:id',
                templateUrl: paths.TEMPLATES + '/views/todo.html',
                controller: 'TodoController',
                controllerAs: 'todo'
            })
            .state('app.todos.add', {
                url: '/add',
                templateUrl: paths.TEMPLATES + '/views/add-todo.html',
                controller: 'AddTodoController',
                controllerAs: 'todo'
            });
        $urlRouterProvider.otherwise('/app/todos');
    }
}());

(function () {
    'use strict';

    angular
        .module('app.core')
        .filter('truncate', truncate);

    truncate.$inject = [];

    function truncate () {
        return function (string, maxLength) {
            var truncated;
            maxLength = maxLength || 30;

            if (typeof string !== 'string') {
                throw 'String required.';
            }

            if (string.length > maxLength) {
                truncated = string.slice(0, maxLength) + '...';
                return truncated;
            } else {
                return string;
            }
        };
    }
}());

(function () {
    'use strict';

    angular
        .module('app.todos')
        .controller('AddTodoController', AddTodoController);

    AddTodoController.$inject = ['$scope', 'Task'];

    function AddTodoController ($scope, Task) {
        var vm = this,
            typingListener;
        vm.typing = false;
        vm.task = new Task();
        vm.createTask = createTask;
        vm.errors = {};

        $scope.scopeFail = '';
        $scope.scopeFailObject = {prop: ''};

        function createTask () {
            if (typeof vm.task !== 'undefined') {
                vm.errors = [];
                if (typeof vm.task.name === 'undefined' ||
                    vm.task.name.length < 1) {
                    vm.errors.push('Name is required');
                }
                if (typeof vm.task.description === 'undefined' ||
                    vm.task.description.length < 1) {
                    vm.errors.push('Description is required');
                }
                if (vm.errors.length === 0) {
                    vm.task.$save(function () {
                        vm.task = new Task();
                    });
                }
            }
        }

        typingListener = $scope.$watch('todo.task.name',
            function (newValue) {
                if (typeof newValue !== 'undefined' && newValue.length > 0) {
                    vm.typing = true;
                } else {
                    vm.typing = false;
                }
            });

        $scope.$on('$destroy', function () {
            typingListener();
        });
    }
}());

(function () {
    'use strict';

    angular
        .module('app.todos')
        .controller('TodoController', TodoController);

    TodoController.$inject = ['Task', '$stateParams'];

    function TodoController (Task, $stateParams) {
        var vm = this;
        vm.task = Task.get({id: $stateParams.id});
    }
}());

(function () {
    'use strict';

    angular
        .module('app.todos')
        .controller('TodosController', TodosController);

    TodosController.$inject = ['Task'];

    function TodosController (Task) {
        var vm = this;
        vm.tasks = Task.query();
        vm.removeTask = removeTask;

        function removeTask (task) {
            task.$delete(function () {
                vm.tasks.splice(vm.tasks.indexOf(task), 1);
            });
        }
    }
}());

(function () {
    'use strict';

    angular
        .module('app.todos')
        .factory('Task', Task);

    Task.$inject = ['$resource', 'paths'];

    function Task ($resource, paths) {
        return $resource(paths.API + '/task/:id', {id: '@_id'});
    }
}());

(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('clock', clock);

    clock.$inject = ['dateFilter', '$timeout'];

    function clock (dateFilter, $timeout) {
        return {
            link: link,
            template: '{{ time }}',
            restrict: 'EA',
            scope: {
                format: '@'
            }
        };

        function link (scope) {
            var updateTime = function () {
                var now = Date.now();

                scope.time = dateFilter(now, scope.format);
                $timeout(updateTime, now % 1000);
            };

            updateTime();
        }
    }
}());
