(function () {
    'use strict';

    angular
        .module('app.core')
        .filter('truncate', truncate);

    truncate.$inject = [];

    function truncate () {
        return function (string, maxLength) {
            var truncated;
            maxLength = maxLength || 30;

            if (typeof string !== 'string') {
                throw 'String required.';
            }

            if (string.length > maxLength) {
                truncated = string.slice(0, maxLength) + '...';
                return truncated;
            } else {
                return string;
            }
        };
    }
}());
