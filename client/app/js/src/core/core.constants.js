(function () {
    'use strict';

    angular
        .module('app.core')
        .constant('paths', {
            TEMPLATES: 'templates',
            API: 'http://localhost:3000'
        });
}());
