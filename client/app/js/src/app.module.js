(function () {
    'use strict';

    angular
        .module('app', [
            /* Shared modules */
            'app.core',
            'app.widgets',

            /* App modules */
            'app.todos'
        ]);
}());
