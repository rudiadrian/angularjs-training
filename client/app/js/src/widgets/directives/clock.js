(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('clock', clock);

    clock.$inject = ['dateFilter', '$timeout'];

    function clock (dateFilter, $timeout) {
        return {
            link: link,
            template: '{{ time }}',
            restrict: 'EA',
            scope: {
                format: '@'
            }
        };

        function link (scope) {
            var updateTime = function () {
                var now = Date.now();

                scope.time = dateFilter(now, scope.format);
                $timeout(updateTime, now % 1000);
            };

            updateTime();
        }
    }
}());
