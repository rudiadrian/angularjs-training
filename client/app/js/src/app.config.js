(function () {
    'use strict';

    angular
        .module('app')
        .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider', 'paths'];

    function configure ($stateProvider, $urlRouterProvider, paths) {
        $stateProvider
            .state('app', {
            url: '/app',
                abstract: true,
                templateUrl: paths.TEMPLATES + '/pages/app.html',
            });

        $urlRouterProvider.otherwise('/app/todos/list');
    }
}());
