(function () {
    'use strict';

    angular
        .module('app.todos')
        .factory('Task', Task);

    Task.$inject = ['$resource', 'paths'];

    function Task ($resource, paths) {
        return $resource(paths.API + '/task/:id', {id: '@_id'});
    }
}());
