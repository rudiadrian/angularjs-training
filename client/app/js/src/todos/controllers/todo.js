(function () {
    'use strict';

    angular
        .module('app.todos')
        .controller('TodoController', TodoController);

    TodoController.$inject = ['Task', '$stateParams'];

    function TodoController (Task, $stateParams) {
        var vm = this;
        vm.task = Task.get({id: $stateParams.id});
    }
}());
