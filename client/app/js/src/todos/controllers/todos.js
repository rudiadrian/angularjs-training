(function () {
    'use strict';

    angular
        .module('app.todos')
        .controller('TodosController', TodosController);

    TodosController.$inject = ['Task'];

    function TodosController (Task) {
        var vm = this;
        vm.tasks = Task.query();
        vm.removeTask = removeTask;

        function removeTask (task) {
            task.$delete(function () {
                vm.tasks.splice(vm.tasks.indexOf(task), 1);
            });
        }
    }
}());
