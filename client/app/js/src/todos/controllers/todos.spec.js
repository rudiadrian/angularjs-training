(function () {
    'use strict';

    describe('TodosController', function () {
        beforeEach(module('app'));

        var $controller;

        beforeEach(inject(function (_$controller_) {
            $controller = _$controller_;
        }));

        describe('vm.addUser', function () {
            var $scope, controller;

            beforeEach(function () {
                $scope = {};
                controller = $controller(
                    'TodosController', {$scope: $scope});
            });

            it('do nothing if user name is undefined', function () {
                var user = {name: undefined};
                controller.users = [];
                controller.addUser(user);
                expect(controller.users.length).toEqual(0);
            });

            it('do nothing if user name is empty', function () {
                var user = {name: ''};
                controller.users = [];
                controller.addUser(user);
                expect(controller.users.length).toEqual(0);
            });

            it('adds user to list if user name is defined and is not empty',
             function () {
                var user = {name: 'John Doe'};
                controller.users = [];
                controller.addUser(user);
                expect(controller.users.length).toEqual(1);
                expect(controller.users[0]).toEqual(user);
            });
        });
    });
}());
