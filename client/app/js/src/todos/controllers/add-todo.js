(function () {
    'use strict';

    angular
        .module('app.todos')
        .controller('AddTodoController', AddTodoController);

    AddTodoController.$inject = ['$scope', 'Task'];

    function AddTodoController ($scope, Task) {
        var vm = this,
            typingListener;
        vm.typing = false;
        vm.task = new Task();
        vm.createTask = createTask;
        vm.errors = {};

        $scope.scopeFail = '';
        $scope.scopeFailObject = {prop: ''};

        function createTask () {
            if (typeof vm.task !== 'undefined') {
                vm.errors = [];
                if (typeof vm.task.name === 'undefined' ||
                    vm.task.name.length < 1) {
                    vm.errors.push('Name is required');
                }
                if (typeof vm.task.description === 'undefined' ||
                    vm.task.description.length < 1) {
                    vm.errors.push('Description is required');
                }
                if (vm.errors.length === 0) {
                    vm.task.$save(function () {
                        vm.task = new Task();
                    });
                }
            }
        }

        typingListener = $scope.$watch('todo.task.name',
            function (newValue) {
                if (typeof newValue !== 'undefined' && newValue.length > 0) {
                    vm.typing = true;
                } else {
                    vm.typing = false;
                }
            });

        $scope.$on('$destroy', function () {
            typingListener();
        });
    }
}());
