(function () {
    'use strict';

    angular
        .module('app.todos')
        .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider', 'paths'];

    function configure ($stateProvider, $urlRouterProvider, paths) {
        $stateProvider
            .state('app.todos', {
                url: '/todos',
                template: '<ui-view/>',
                abstract: true
            })
            .state('app.todos.list', {
                url: '/list',
                templateUrl: paths.TEMPLATES + '/views/todos.html',
                controller: 'TodosController',
                controllerAs: 'todos'
            })
            .state('app.todos.detail', {
                url: '/detail/:id',
                templateUrl: paths.TEMPLATES + '/views/todo.html',
                controller: 'TodoController',
                controllerAs: 'todo'
            })
            .state('app.todos.add', {
                url: '/add',
                templateUrl: paths.TEMPLATES + '/views/add-todo.html',
                controller: 'AddTodoController',
                controllerAs: 'todo'
            });
        $urlRouterProvider.otherwise('/app/todos');
    }
}());
