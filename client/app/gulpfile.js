var	argv = require('yargs').argv,
	gulp = require('gulp'),
	concat = require('gulp-concat'),
	changed = require('gulp-changed'),
	uglify = require('gulp-uglify'),
	gulpif = require('gulp-if'),
    ignore = require('gulp-ignore'),
	stripDebug = require('gulp-strip-debug'),
	jshint = require('gulp-jshint'),
	jscs = require('gulp-jscs'),
	karma = require('karma').server,
    compass = require('gulp-compass'),
    minifyCSS = require('gulp-minify-css'),
    bower = require('gulp-bower'),
    sequence = require('gulp-sequence'),
	production = !!(argv.production), // true if --production flag is used
	SCRIPTS_VENDOR_PATH = 'js/vendor',
	SCRIPTS_SOURCE_PATH = 'js/src',
	SCRIPTS_DIST_PATH = '../dist/js',
	STYLES_SOURCE_PATH = 'sass',
	STYLES_DIST_PATH = '../dist/css',
	vendorScripts,
	source,
	build;

vendorScripts = [
	SCRIPTS_VENDOR_PATH + '/angular/angular.js',
	SCRIPTS_VENDOR_PATH + '/angular-ui-router/release/angular-ui-router.js',
	SCRIPTS_VENDOR_PATH + '/angular-resource/angular-resource.js'
];

source = {
	scripts: {
		app: [
			SCRIPTS_SOURCE_PATH + '/**/*.module.js',
			SCRIPTS_SOURCE_PATH + '/**/*.constants.js',
			SCRIPTS_SOURCE_PATH + '/**/*.js'
		],
		vendor: vendorScripts,
		watch: [SCRIPTS_SOURCE_PATH + '/**/*.js']
	},
    styles: {
        main: STYLES_SOURCE_PATH + '/**/*.scss',
        watch: [STYLES_SOURCE_PATH + '/**/*.scss']
    }
};

build = {
	scripts: {
		app: {
			main: 'app.js',
			dir: SCRIPTS_DIST_PATH
		},
		vendor: {
			main: 'base.js',
			dir: SCRIPTS_DIST_PATH
		}
	},
    styles: {
        main: {
            dir: STYLES_DIST_PATH
        }
    }
};

gulp.task('scripts:bower', function () {
	return bower();
});

gulp.task('scripts:vendor', function () {
	return gulp.src(source.scripts.vendor)
		.pipe(concat(build.scripts.vendor.main))
		.pipe(gulp.dest(build.scripts.vendor.dir))
});

gulp.task('scripts:app', function () {
	return gulp.src(source.scripts.app)
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(jshint.reporter('fail'))
		.pipe(jscs())
        .pipe(ignore.exclude('*.spec.js'))
		.pipe(concat(build.scripts.app.main))
		.pipe(gulpif(production, stripDebug()))
		.pipe(gulpif(production, uglify()))
		.pipe(gulp.dest(build.scripts.app.dir));
});

gulp.task('styles:main', function () {
   return gulp.src(source.styles.main)
       .pipe(compass({
            css: build.styles.main.dir,
            sass: source.styles.main.dir
       }))
       .pipe(gulpif(production, minifyCSS()))
       .pipe(gulp.dest(build.styles.main.dir));
});

gulp.task('watch', function () {
	gulp.watch(source.scripts.watch, ['scripts:app']);
    gulp.watch(source.styles.watch, ['styles:main']);
});

gulp.task('test', function (done) {
	karma.start({
		configFile: __dirname + '/karma.conf.js'
	}, done);
});

gulp.task('default', sequence(
	'scripts:bower',
	'scripts:vendor',
	[
		'scripts:app',
    	'styles:main'
	],
	'watch'
));

gulp.task('jenkins', sequence(
	'scripts:bower',
	'scripts:vendor',
	[
		'scripts:app',
    	'styles:main'
	]
));