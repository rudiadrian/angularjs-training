var express = require('express'),
    restful = require('node-restful'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    mongoose = restful.mongoose

// Make a new Express app
var app = module.exports = express();

// Connect to mongodb
mongoose.connect("mongodb://localhost:27018/angular_training");

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

// Use middleware to parse POST data and use custom HTTP methods
app.use(bodyParser());
app.use(methodOverride());
app.use(allowCrossDomain);

var Task = restful.model( "tasks", mongoose.Schema({
    name: 'string',
    description: 'string',
  }))
  .methods(['get', 'put', 'delete', 'post']);

Task.register(app, '/task'); // Register the user model at the localhost:3000/task

app.listen(3000);